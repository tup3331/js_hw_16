const titles = document.querySelectorAll(".tabs-title");
const titlesArr = Array.from(titles);

const contents = document.querySelectorAll(".tabs-content li")
const contentsArr = Array.from(contents);

console.log(contentsArr)

let curContent = 0;

titles.forEach((title, content) => {
    title.addEventListener("click", () => {
        curContent = content;
        let titleIndex = titlesArr.indexOf(title);
        let contentIndex = contentsArr[titleIndex];
        setActive(contentIndex, title);
    })
});

function setActive(arrPar, arrTitle) {
    contents.forEach((par) => {
        if (par === arrPar) {
            par.classList.add("active")
        } else {
            par.classList.remove("active");
        }

    titles.forEach((title) => {
        if (title === arrTitle) {
            title.classList.add("active")
        } else {
            title.classList.remove("active")
        }
    })
    })
}